<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="generator" content="HTML Tidy for HTML5 for Linux version 5.3.12">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="author" content="Christian Pujol">
  <meta name="student_id" content="15611221">
  <meta name="description" content="A tribute webpage to Fridtjof Nansen">
  <title>naga.no | ied | Fridtjof Nansen</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <script>
    function changecolor(color) {
      let i;
      colored = document.getElementsByClassName("color");
      for (i = 0; i < colored.length; i++) {
        colored[i].style["background-color"] = color;
      }
    }

    // the next 10 lines are shamelessly inspired (copied?)
    //  from the modal tutorial from w3school.com.
    //  https://www.w3schools.com/howto/howto_css_modal_images.asp

    function showModal(img) {
      var modal = document.getElementById('modal');
      var modalImg = document.getElementById("modal-img");
      var caption = document.getElementById("caption");
      modal.style.display = "block";
      modalImg.src = img.src;
      caption.innerHTML = img.alt;
    }

    var closeModal= () => document.getElementById('modal').style.display = 'none';
  </script>
</head>

<body>
<div id="modal" class="modal">
  <img class="modal-content" id="modal-img" onclick="closeModal()">
  <div id="caption"></div>
</div>
  <nav  class="color">
<br>
Chose your colors:
 <select name="colors" onchange="changecolor(this.value)">
  <option value="#d9edf7">cyan</option>
  <option value="red">red</option>
  <option value="green">green</option>
  <option value="yellow">yellow</option>
</select> 
  </nav>
  <section>
    <div class="grey rounded clearfix">
      <div class="float left third">
        <img class="rounded" src="Fridtjof_Nansen_Portrait.jpg" alt="Fridtjof Nansen's portrait" onclick="showModal(this)">
      </div>
      <div class="float right two-third">
        <h1>Fridtjof Nansen <small>(1861–1930)</small></h1>
        <div class="rounded color">
          <blockquote cite="https://www.nobelprize.org/nobel_prizes/peace/laureates/1922/nansen-lecture.html">
            <p>"No future, however, can be built on despair, distrust, hatred, and envy."</p><a href="http://www.nobelprize.org/nobel_prizes/peace/laureates/1922/nansen-lecture.html" target="_blank"><cite>Nobel Lecture,
          19.12.1922</cite></a>
            <footer>
              Fridtjof Nansen
            </footer>
          </blockquote>
        </div>
      </div>
    </div>
    <main class="rounded grey">
      <section class="rounded float left half">
        <h3>Scientist and polar explorer</h3><img class="rounded" src="Nansen_14_march_95.jpg" alt="Bidding farewell to the «Fram» and the crew, as Fridtjof Nansen and H. Johansen set out on their expedition toward the North Pole." onclick="showModal(this)">
        <div class="rounded color">
          <blockquote>
            <p>"The most important thing in all research is not the results, the final conclusion, but the research itself, the struggle to achieve the results."</p>
            <footer>
              Fridtjof Nansen
            </footer>
          </blockquote>
        </div>
        <ul class="rounded color">
          <li><strong>1881</strong> Began to study zoology at the University
          </li>
          <li><strong>1882</strong> Five month Arctic's sea voyage on board of the <em>Viking</em>.</li>
          <li><strong>1887</strong> Doctoral thesis:<cite>Structure
        and Combination of Histological Elements of the Central
        Nervous System</cite>.</li>
          <li><strong>1888</strong> Lead the first Crossing of Greenland interior.</li>
          <li>
            <strong>1893–1896</strong> Drift through the ices of the Arctic ocean with the <a href="https://en.wikipedia.org/wiki/Fram"><em>Fram</em></a>
          </li>
          <li>
            <strong>1895</strong>
            <ul>
              <li><strong>14 March</strong> Nansen and Hjalmar Johansen leave the <em>Fram</em> and try to reach the north pole, distant of 660 km.</li>
              <li><strong>7 April</strong> Nansen turn south, after reaching 86°13.6′N, three degrees beyond the previous Farthest North mark.</li>
            </ul>
          </li>
        </ul>
      </section>
      <section class="float right half">
        <h3>Diplomat and humanitarian</h3><img class="rounded" src="Nas_food.jpg" alt="Fridtjof Nansen testing food at mealtimes of orphan boys in a summer camp." onclick="showModal(this)">
        <div class="rounded color">
          <blockquote>
            <p>"If nations […] could meet with confidence […], they would easily be able to establish a lasting peace."</p>
            <footer>
              Fridtjof Nansen
            </footer>
          </blockquote>
        </div>
        <ul class="rounded color">
          <li><strong>1906–1908</strong> Norway's ambassador in London.
          </li>
          <li><strong>1920–1930</strong> Norway's delegate at the League of Nations general Assembly.</li>
          <li><strong>1920</strong> At the League of nation's request, Nansen began organising the repatriation of around half a million prisoners of war.</li>
          <li><strong>1921</strong> Appointed the first High Commissioner for Refugees by the League of Nations.</li>
          <li><strong>1921–1922</strong> Nansen organize a relief programme for millions of victims of the Russian Famine of 1921-1922.
          </li>
          <li>
            <strong>1922</strong> The first <a href="https://en.wikipedia.org/wiki/Nansen_passport">Nansen
          passports</a> are issued.
          </li>
          <li><strong>1922</strong> Nobel Peace Prize, in recognition of his work for refugees and the famine-stricken.</li>
          <li><strong>1925–1927</strong> Work for helping the Armenian's refugees.</li>
        </ul>
      </section>
      <div class="clear"></div>
    </main>
    <footer class="rounded grey">
      <h3 id="know-more">Want to know more about Fridtjof
    Nansen?</h3>
      <ul class="rounded color">
        <li>
          <a href="https://en.wikipedia.org/wiki/Fridtjof_Nansen">Wikipedia
        entry</a> about Fridtjof Nansen.
        </li>
        <li>
          <a href="http://www.badassoftheweek.com/nansen.html">Badass
        of the week: Fridtjof Nansen.</a>
        </li>
        <li>The Nobel Peace Prize 1922:
          <ul>
            <li>
              <a href="http://www.nobelprize.org/nobel_prizes/peace/laureates/1922/press.html">
            Award Ceremony Speech.</a>
            </li>
            <li>
              <a href="http://www.nobelprize.org/nobel_prizes/peace/laureates/1922/nansen-lecture.html">
            Nobel Lecture.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="http://frammuseum.no/polar_history/explorers/fridtjof_nansen__1861-1930">
        Fridtjof Nansen</a> polar explorer at <a href="http://www.frammuseum.no">frammuseum.no</a>.
        </li>
        <li>
          <a href="http://www.unhcr.org/events/nansen/4aae50086/nansen-man-action-vision.html?query=nansen">
        Nansen - a man of action and vision</a> at the United Nations High Commissioner for Refugees <a href="http://www.unhcr.org">website</a>.
        </li>
      </ul>
    </footer>
  </section>
  <section id="comment">
    <!-- add a form -->
    <div class="comment grey clearfix rounded">
      <h3> Comments</h3>
      <div class="color new-comment half float rounded">
        <h4> Add a comment:</h4>
        <form method="post" action="index.php">
          <label>
          <strong>Name</strong>
          <br/>
            <input type="text" name="name"/>
          </label>
          <br/>
          <label>
            <strong>Your comment</strong><small> (max. 140 char)</small>
          <br/>
            <textarea col=40 maxlength=140 row=5 name="comment"> </textarea>
          </label>
          <br/>
          <input value="Submit" type="submit" />
        </form>
      </div>
      <div class="color comment-list half float rounded">

        <!-- add a list of comment and save add the new comment to database -->
        <ol>

       <?php

         # Inspiré de w3schools.net
         function nettoyer($x){
           if ($x){
             $x = trim($x);
             $x = stripslashes($x);
             $x = htmlspecialchars($x);
           }
           return $x;
         }

         $nocomment = true;
         $server = "localhost";
         $user = "user";
         $passe = "mypasswd";
         $base = "mydb";

         $con = mysqli_connect($server, $user, $passe, $base);

             if (! $con)
             { 
               echo "No connection : " . mysqli_connect_error();
             } else {
            
                if ($_POST) {
                  $name = nettoyer($_POST['name']);
                  $text = nettoyer($_POST['comment']);
                  if ($res = mysqli_query($con,
                      "INSERT INTO `com` (`id`, `name`, `date`, `text`)
                       VALUES (NULL, '$name', CURRENT_TIMESTAMP, '$text')")) {
                         echo "<p><strong>Thanks</strong> for your comment " . $name . "!!</p>";
                  } else {
                         echo "<p><strong>OUPS !!</strong>
                              something ate your comment " . $name . "!!</p>";
                  }
                }

               if ($res = mysqli_query($con, "SELECT * FROM com ;")) {
                 while($ligne = mysqli_fetch_assoc($res)) {
                     $name = $ligne['name'];
                     $date = $ligne['date'];
                     $text = $ligne['text'];
                     $nocomment = false;

                     echo "<li class='rounded'>";
                     echo '<blockquote class="comment">';
                     echo "<p>$text</p>";
                     echo "</blockquote>";
                     echo "<footer><p>$name, the $date</p></footer>";
                     echo "<hr/>";
                     echo "</li>";
                 }
                 if ($nocomment) echo "<p>no comment yet</p>";
                }
                 
                else
                    echo "<p>Aie ! " . mysqli_error($con) . "</p>";
             }
         ?>
        </ol>
      </div>
    </div>
  </section>
</body>

</html>
