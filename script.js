function changecolor(color) {
  let i;
  colored = document.getElementsByClassName("color");
  for (i = 0; i < colored.length; i++) {
    colored[i].style["background-color"] = color;
  }
}

// the next 10 lines are shamelessly inspired (copied?)
//  from the modal tutorial from w3school.com.
//  https://www.w3schools.com/howto/howto_css_modal_images.asp

function showModal(img) {
  var modal = document.getElementById('modal');
  var modalImg = document.getElementById("modal-img");
  var caption = document.getElementById("caption");
  modal.style.display = "block";
  modalImg.src = img.src;
  caption.innerHTML = img.alt;
}

var closeModal= () => document.getElementById('modal').style.display = 'none';

